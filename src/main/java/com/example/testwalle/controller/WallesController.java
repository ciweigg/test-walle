package com.example.testwalle.controller;

import com.example.testwalle.ServerConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2019/6/15.
 */
@RestController
@Slf4j
public class WallesController {

	@Autowired
	private ServerConfig serverConfig;

	@RequestMapping("/walle")
	public String walle(){
		log.info("测试info日志");
		log.error("测试error日志");
		log.warn("测试warn日志");
		log.debug("测试debug日志");
		return "build walle success2 with jenkinsfile " + serverConfig.getUrl();
	}

}
