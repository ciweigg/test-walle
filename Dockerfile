FROM maven:3.6.0-jdk-8-slim AS build
COPY pom.xml /app/
COPY src /app/src
COPY settings.xml /app/
RUN mvn -s /app/settings.xml -f /app/pom.xml clean package

FROM java:8
COPY --from=build /app/target/test-walle-0.0.1.jar app.jar
ENTRYPOINT ["java","-jar","-Xms512M","-Xmx512M","-Xmn256M","-Xss256K","-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005","/app.jar"]
EXPOSE 8080
CMD ["hello worlds"]
